package chaincode

import (
	"encoding/json"
	"errors"
	"fmt"

	"github.com/hyperledger/fabric/core/chaincode/shim"
)

func (t *SmartContractChaincode) createContract(stub shim.ChaincodeStubInterface, args []string) ([]byte, error) {

	/*		0
		json
	  	{
			"ticker":  "string",
			"par": 0.00,
			"qty": 10,
			"discount": 7.5,
			"maturity": 30,
			"validated": false
			"owners": [ // This one is not required
				{
					"company": "company1",
					"quantity": 5
				},
				{
					"company": "company3",
					"quantity": 3
				},
				{
					"company": "company4",
					"quantity": 2
				}
			],
			"issuer":"company2",
			"issueDate":"1456161763790"  (current time in milliseconds as a string)

		}
	*/
	//need one arg
	if len(args) != 1 {
		fmt.Println("error invalid arguments")
		return nil, errors.New("Incorrect number of arguments. Expecting contract data")
	}

	var sc SmartContract
	var err error
	var account Account

	fmt.Println("Unmarshalling SC")
	err = json.Unmarshal([]byte(args[0]), &sc)
	if err != nil {
		fmt.Println("Error invalid contract create data")
		return nil, errors.New("Invalid contract create data")
	}

	//generate the CUSIP
	fmt.Println("Getting state of - " + accountPrefix + sc.Issuer)
	accountBytes, err := stub.GetState(accountPrefix + sc.Issuer)
	if err != nil {
		fmt.Println("Error Getting state of - " + accountPrefix + sc.Issuer)
		return nil, errors.New("Error retrieving account " + sc.Issuer)
	}
	err = json.Unmarshal(accountBytes, &account)
	if err != nil {
		fmt.Println("Error Unmarshalling accountBytes")
		return nil, errors.New("Error retrieving account " + sc.Issuer)
	}

	// Set the issuer to be the owner of all quantity
	var owner Owner
	owner.Company = sc.Issuer
	owner.Quantity = sc.Qty

	sc.Owners = append(sc.Owners, owner)
	sc.Validated = false
	suffix, err := generateCUSIPSuffix(sc.IssueDate, sc.Maturity)
	if err != nil {
		fmt.Println("Error generating cusip")
		return nil, errors.New("Error generating CUSIP")
	}

	fmt.Println("Marshalling CP bytes")
	sc.CUSIP = account.Prefix + suffix + sc.Ticker

	if len(account.AssetsIds) == 1 && account.AssetsIds[0] == "" {
		account.AssetsIds[0] = sc.CUSIP
	} else {
		account.AssetsIds = append(account.AssetsIds, sc.CUSIP)
	}

	fmt.Println("Getting State on CP " + sc.CUSIP)
	cpRxBytes, err := stub.GetState(scPrefix + sc.CUSIP)
	if cpRxBytes == nil {
		fmt.Println("CUSIP does not exist, creating it")
		cpBytes, err := json.Marshal(&sc)
		if err != nil {
			fmt.Println("Error marshalling cp")
			return nil, errors.New("Error creating contract")
		}
		err = stub.PutState(scPrefix+sc.CUSIP, cpBytes)
		if err != nil {
			fmt.Println("Error creating contract")
			return nil, errors.New("Error creating contract")
		}

		fmt.Println("Marshalling account bytes to write")
		accountBytesToWrite, err := json.Marshal(&account)
		if err != nil {
			fmt.Println("Error marshalling account")
			return nil, errors.New("Error creating contract")
		}
		err = stub.PutState(accountPrefix+sc.Issuer, accountBytesToWrite)
		if err != nil {
			fmt.Println("Error putting state on accountBytesToWrite")
			return nil, errors.New("Error creating contract")
		}

		// Update the paper keys by adding the new key
		fmt.Println("Getting Contract Keys")
		keysBytes, err := stub.GetState("SmartContractKeys")
		if err != nil {
			fmt.Println("Error retrieving contract keys")
			return nil, errors.New("Error retrieving Contract keys")
		}
		var keys []string
		err = json.Unmarshal(keysBytes, &keys)
		if err != nil {
			fmt.Println("Error unmarshel keys")
			return nil, errors.New("Error unmarshalling Contract keys ")
		}

		fmt.Println("Appending the new key to Contract Keys")
		foundKey := false
		for _, key := range keys {
			if key == scPrefix+sc.CUSIP {
				foundKey = true
			}
		}
		if foundKey == false {
			keys = append(keys, scPrefix+sc.CUSIP)
			keysBytesToWrite, err := json.Marshal(&keys)
			if err != nil {
				fmt.Println("Error marshalling keys")
				return nil, errors.New("Error marshalling the keys")
			}
			fmt.Println("Put state on SmartContractKeys")
			err = stub.PutState("SmartContractKeys", keysBytesToWrite)
			if err != nil {
				fmt.Println("Error writting keys back")
				return nil, errors.New("Error writing the keys back")
			}
		}

		fmt.Println("create contract %+v\n", sc)
		return nil, nil
	}
	fmt.Println("CUSIP exists")

	var cprx SmartContract
	fmt.Println("Unmarshalling CP " + sc.CUSIP)
	err = json.Unmarshal(cpRxBytes, &cprx)
	if err != nil {
		fmt.Println("Error unmarshalling cp " + sc.CUSIP)
		return nil, errors.New("Error unmarshalling cp " + sc.CUSIP)
	}

	cprx.Qty = cprx.Qty + sc.Qty

	for key, val := range cprx.Owners {
		if val.Company == sc.Issuer {
			cprx.Owners[key].Quantity += sc.Qty
			break
		}
	}

	cpWriteBytes, err := json.Marshal(&cprx)
	if err != nil {
		fmt.Println("Error marshalling cp")
		return nil, errors.New("Error creating contract")
	}
	err = stub.PutState(scPrefix+sc.CUSIP, cpWriteBytes)
	if err != nil {
		fmt.Println("Error issuing paper")
		return nil, errors.New("Error creating contract")
	}

	fmt.Println("Updated contract %+v\n", cprx)
	return nil, nil

}

// GetAllContracts query
func GetAllContracts(stub shim.ChaincodeStubInterface) ([]SmartContract, error) {

	var allCPs []SmartContract

	// Get list of all the keys
	keysBytes, err := stub.GetState("SmartContractKeys")
	if err != nil {
		fmt.Println("Error retrieving paper keys")
		return nil, errors.New("Error retrieving paper keys")
	}
	var keys []string
	err = json.Unmarshal(keysBytes, &keys)
	if err != nil {
		fmt.Println("Error unmarshalling paper keys")
		return nil, errors.New("Error unmarshalling paper keys")
	}

	// Get all the cps
	for _, value := range keys {
		cpBytes, err := stub.GetState(value)

		var cp SmartContract
		err = json.Unmarshal(cpBytes, &cp)
		if err != nil {
			fmt.Println("Error retrieving cp " + value)
			return nil, errors.New("Error retrieving cp " + value)
		}

		fmt.Println("Appending CP" + value)
		allCPs = append(allCPs, cp)
	}

	return allCPs, nil
}

// GetContract query
func GetContract(cpid string, stub shim.ChaincodeStubInterface) (SmartContract, error) {
	var cp SmartContract

	cpBytes, err := stub.GetState(cpid)
	if err != nil {
		fmt.Println("Error retrieving contract " + cpid)
		return cp, errors.New("Error retrieving cp " + cpid)
	}

	err = json.Unmarshal(cpBytes, &cp)
	if err != nil {
		fmt.Println("Error unmarshalling contract " + cpid)
		return cp, errors.New("Error unmarshalling contract " + cpid)
	}

	return cp, nil
}

func (t *SmartContractChaincode) validateContract(stub shim.ChaincodeStubInterface, args []string) ([]byte, error) {
	if len(args) != 1 {
		return nil, errors.New("Incorrect number of arguments. Expecting contract number")
	}

	var CUSIP = args[0]

	cpBytes, err := stub.GetState(scPrefix + CUSIP)
	if err != nil {
		fmt.Println("CUSIP not found")
		return nil, errors.New("CUSIP not found " + CUSIP)
	}

	var cp SmartContract
	fmt.Println("Unmarshalling CP " + CUSIP)
	err = json.Unmarshal(cpBytes, &cp)
	if err != nil {
		fmt.Println("Error unmarshalling cp " + CUSIP)
		return nil, errors.New("Error unmarshalling cp " + CUSIP)
	}

	cp.Validated = true

	contractBytesToWrite, err := json.Marshal(&cp)
	if err != nil {
		fmt.Println("Error marshalling the toCompany")
		return nil, errors.New("Error marshalling the toCompany")
	}
	fmt.Println("Put state on toCompany")
	err = stub.PutState(scPrefix+CUSIP, contractBytesToWrite)
	if err != nil {
		fmt.Println("Error marshalling the contract")
		return nil, errors.New("Error marshalling contract")
	}
	return nil, nil
}

// Still working on this one
func (t *SmartContractChaincode) transferContract(stub shim.ChaincodeStubInterface, args []string) ([]byte, error) {
	/*		0
		json
	  	{
			  "CUSIP": "",
			  "fromCompany":"",
			  "toCompany":"",
			  "quantity": 1
		}
	*/
	//need one arg
	if len(args) != 1 {
		return nil, errors.New("Incorrect number of arguments. Expecting contract number")
	}

	var tr Transaction

	fmt.Println("Unmarshalling Transaction")
	err := json.Unmarshal([]byte(args[0]), &tr)
	if err != nil {
		fmt.Println("Error Unmarshalling Transaction")
		return nil, errors.New("Invalid contract")
	}

	fmt.Println("Getting State on CP " + tr.CUSIP)
	cpBytes, err := stub.GetState(scPrefix + tr.CUSIP)
	if err != nil {
		fmt.Println("CUSIP not found")
		return nil, errors.New("CUSIP not found " + tr.CUSIP)
	}

	var cp SmartContract
	fmt.Println("Unmarshalling CP " + tr.CUSIP)
	err = json.Unmarshal(cpBytes, &cp)
	if err != nil {
		fmt.Println("Error unmarshalling cp " + tr.CUSIP)
		return nil, errors.New("Error unmarshalling cp " + tr.CUSIP)
	}

	var fromCompany Account
	fmt.Println("Getting State on fromCompany " + tr.FromCompany)
	fromCompanyBytes, err := stub.GetState(accountPrefix + tr.FromCompany)
	if err != nil {
		fmt.Println("Account not found " + tr.FromCompany)
		return nil, errors.New("Account not found " + tr.FromCompany)
	}

	fmt.Println("Unmarshalling FromCompany ")
	err = json.Unmarshal(fromCompanyBytes, &fromCompany)
	if err != nil {
		fmt.Println("Error unmarshalling account " + tr.FromCompany)
		return nil, errors.New("Error unmarshalling account " + tr.FromCompany)
	}

	var toCompany Account
	fmt.Println("Getting State on ToCompany " + tr.ToCompany)
	toCompanyBytes, err := stub.GetState(accountPrefix + tr.ToCompany)
	if err != nil {
		fmt.Println("Account not found " + tr.ToCompany)
		return nil, errors.New("Account not found " + tr.ToCompany)
	}

	fmt.Println("Unmarshalling tocompany")
	err = json.Unmarshal(toCompanyBytes, &toCompany)
	if err != nil {
		fmt.Println("Error unmarshalling account " + tr.ToCompany)
		return nil, errors.New("Error unmarshalling account " + tr.ToCompany)
	}

	// Check for all the possible errors
	ownerFound := false
	quantity := 0
	for _, owner := range cp.Owners {
		if owner.Company == tr.FromCompany {
			ownerFound = true
			quantity = owner.Quantity
		}
	}

	// If fromCompany doesn't own this paper
	if ownerFound == false {
		fmt.Println("The company " + tr.FromCompany + "doesn't own any of this paper")
		return nil, errors.New("The company " + tr.FromCompany + "doesn't own any of this paper")
	}
	fmt.Println("The FromCompany does own this contract")

	// If fromCompany doesn't own enough quantity of this paper
	if quantity < tr.Quantity {
		fmt.Println("The company " + tr.FromCompany + "doesn't own enough of this paper")
		return nil, errors.New("The company " + tr.FromCompany + "doesn't own enough of this paper")
	}
	fmt.Println("The FromCompany owns enough of this paper")

	amountToBeTransferred := float64(tr.Quantity) * cp.Par
	amountToBeTransferred -= (amountToBeTransferred) * (cp.Discount / 100.0) * (float64(cp.Maturity) / 360.0)

	// If toCompany doesn't have enough cash to buy the papers
	if toCompany.CashBalance < amountToBeTransferred {
		fmt.Println("The company " + tr.ToCompany + "doesn't have enough cash to purchase the papers")
		return nil, errors.New("The company " + tr.ToCompany + "doesn't have enough cash to purchase the papers")
	}
	fmt.Println("The ToCompany has enough money to be transferred for this paper")

	toCompany.CashBalance -= amountToBeTransferred
	fromCompany.CashBalance += amountToBeTransferred

	toOwnerFound := false
	for key, owner := range cp.Owners {
		if owner.Company == tr.FromCompany {
			fmt.Println("Reducing Quantity from the FromCompany")
			cp.Owners[key].Quantity -= tr.Quantity
			//			owner.Quantity -= tr.Quantity
		}
		if owner.Company == tr.ToCompany {
			fmt.Println("Increasing Quantity from the ToCompany")
			toOwnerFound = true
			cp.Owners[key].Quantity += tr.Quantity
			//			owner.Quantity += tr.Quantity
		}
	}

	if toOwnerFound == false {
		var newOwner Owner
		fmt.Println("As ToOwner was not found, appending the owner to the CP")
		newOwner.Quantity = tr.Quantity
		newOwner.Company = tr.ToCompany
		cp.Owners = append(cp.Owners, newOwner)
	}

	fromCompany.AssetsIds = append(fromCompany.AssetsIds, tr.CUSIP)

	// Write everything back
	// To Company
	toCompanyBytesToWrite, err := json.Marshal(&toCompany)
	if err != nil {
		fmt.Println("Error marshalling the toCompany")
		return nil, errors.New("Error marshalling the toCompany")
	}
	fmt.Println("Put state on toCompany")
	err = stub.PutState(accountPrefix+tr.ToCompany, toCompanyBytesToWrite)
	if err != nil {
		fmt.Println("Error writing the toCompany back")
		return nil, errors.New("Error writing the toCompany back")
	}

	// From company
	fromCompanyBytesToWrite, err := json.Marshal(&fromCompany)
	if err != nil {
		fmt.Println("Error marshalling the fromCompany")
		return nil, errors.New("Error marshalling the fromCompany")
	}
	fmt.Println("Put state on fromCompany")
	err = stub.PutState(accountPrefix+tr.FromCompany, fromCompanyBytesToWrite)
	if err != nil {
		fmt.Println("Error writing the fromCompany back")
		return nil, errors.New("Error writing the fromCompany back")
	}

	// cp
	cpBytesToWrite, err := json.Marshal(&cp)
	if err != nil {
		fmt.Println("Error marshalling the cp")
		return nil, errors.New("Error marshalling the cp")
	}
	fmt.Println("Put state on CP")
	err = stub.PutState(scPrefix+tr.CUSIP, cpBytesToWrite)
	if err != nil {
		fmt.Println("Error writing the cp back")
		return nil, errors.New("Error writing the cp back")
	}

	fmt.Println("Successfully completed Invoke")
	return nil, nil
}

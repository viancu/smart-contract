package chaincode

import (
	"encoding/json"
	"errors"
	"fmt"

	"github.com/hyperledger/fabric/core/chaincode/shim"
)

var scPrefix = "sc:"
var accountPrefix = "acct:"
var accountsKey = "accounts"

// SmartContractChaincode example simple Chaincode implementation
type SmartContractChaincode struct {
}

// Init chaincode
func (t *SmartContractChaincode) Init(stub shim.ChaincodeStubInterface, function string, args []string) ([]byte, error) {
	if function == "init" {
		fmt.Println("Init chaincode")
		return t.init(stub, args)
	}
	return nil, nil
}

// Invoke chaincode
func (t *SmartContractChaincode) Invoke(stub shim.ChaincodeStubInterface, function string, args []string) ([]byte, error) {
	fmt.Println("run is running " + function)

	if function == "createContract" {
		fmt.Println("Firing createContract")
		return t.createContract(stub, args)
	} else if function == "transferContract" {
		fmt.Println("Firing transferContract")
		return t.transferContract(stub, args)
	} else if function == "validateContract" {
		fmt.Println("Firing validateContract")
		return t.validateContract(stub, args)
	} else if function == "createAccount" {
		fmt.Println("Firing createAccount")
		return t.createAccount(stub, args)
	} else if function == "init" {
		fmt.Println("Firing init")
		return t.init(stub, args)
	}

	return nil, errors.New("Received unknown function invocation")
}

// Query chaincode
func (t *SmartContractChaincode) Query(stub shim.ChaincodeStubInterface, function string, args []string) ([]byte, error) {
	if function == "getAllContracts" {
		fmt.Println("Getting all contracts")
		allCPs, err := GetAllContracts(stub)
		if err != nil {
			fmt.Println("Error from GetAllContracts")
			return nil, err
		}

		allCPsBytes, err1 := json.Marshal(&allCPs)
		if err1 != nil {
			fmt.Println("Error marshalling allcps")
			return nil, err1
		}
		fmt.Println("All success, returning allcps")
		return allCPsBytes, nil

	} else if function == "getContract" {
		fmt.Println("Getting particular contract")
		cp, err := GetContract(args[0], stub)
		if err != nil {
			fmt.Println("Error Getting particular contract")
			return nil, err
		}
		cpBytes, err1 := json.Marshal(&cp)
		if err1 != nil {
			fmt.Println("Error marshalling contract")
			return nil, err1
		}
		fmt.Println("All success, returning contract")
		return cpBytes, nil

	} else if function == "getAccount" {
		fmt.Println("Getting account")
		company, err := GetAccount(args[0], stub)
		if err != nil {
			fmt.Println("Error from GetAccount")
			return nil, err
		}
		companyBytes, err1 := json.Marshal(&company)
		if err1 != nil {
			fmt.Println("Error marshalling the account")
			return nil, err1
		}
		fmt.Println("All success, returning the account")
		return companyBytes, nil

	} else if function == "generic" {
		fmt.Println("Generic Query call")
		bytes, err := stub.GetState(args[0])

		fmt.Println(bytes)
		if err != nil {
			fmt.Println("Some error happenend")
			return nil, errors.New("Some Error happened")
		}

		fmt.Println("All success, returning from generic")
		return bytes, nil
	}
	return nil, errors.New("Query method not implemented")
}

func (t *SmartContractChaincode) init(stub shim.ChaincodeStubInterface, args []string) ([]byte, error) {
	fmt.Println("Initializing contract keys collection")
	var blank []string
	blankBytes, _ := json.Marshal(&blank)
	err := stub.PutState("SmartContractKeys", blankBytes)
	if err != nil {
		fmt.Println("Failed to initialize contract keys collection")
	}

	fmt.Println("Initialization complete")
	return nil, nil
}

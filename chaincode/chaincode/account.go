package chaincode

import (
	"encoding/json"
	"errors"
	"fmt"
	"strings"

	"github.com/hyperledger/fabric/core/chaincode/shim"
)

func (t *SmartContractChaincode) createAccount(stub shim.ChaincodeStubInterface, args []string) ([]byte, error) {
	// Obtain the username to associate with the account
	if len(args) != 1 {
		fmt.Println("Error obtaining username")
		return nil, errors.New("createAccount accepts a single username argument")
	}
	username := args[0]

	// Build an account object for the user
	var assetIds []string
	suffix := "000A"
	prefix := username + suffix
	var account = Account{ID: username, Prefix: prefix, CashBalance: 10000000.0, AssetsIds: assetIds}
	accountBytes, err := json.Marshal(&account)
	if err != nil {
		fmt.Println("error creating account" + account.ID)
		return nil, errors.New("Error creating account " + account.ID)
	}

	fmt.Println("Attempting to get state of any existing account for " + account.ID)
	existingBytes, err := stub.GetState(accountPrefix + account.ID)
	if err == nil {

		var company Account
		err = json.Unmarshal(existingBytes, &company)
		if err != nil {
			fmt.Println("Error unmarshalling account " + account.ID + "\n--->: " + err.Error())

			if strings.Contains(err.Error(), "unexpected end") {
				fmt.Println("No data means existing account found for " + account.ID + ", initializing account.")
				err = stub.PutState(accountPrefix+
					account.ID, accountBytes)

				if err == nil {
					fmt.Println("created account" + accountPrefix + account.ID)
					return nil, nil
				}
				fmt.Println("failed to create initialize account for " + account.ID)
				return nil, errors.New("failed to initialize an account for " + account.ID + " => " + err.Error())

			}
			return nil, errors.New("Error unmarshalling existing account " + account.ID)

		}
		fmt.Println("Account already exists for " + account.ID + " " + company.ID)
		return nil, errors.New("Can't reinitialize existing user " + account.ID)

	}

	fmt.Println("No existing account found for " + account.ID + ", initializing account.")
	err = stub.PutState(accountPrefix+account.ID, accountBytes)

	if err == nil {
		fmt.Println("created account" + accountPrefix + account.ID)
		return nil, nil
	}

	fmt.Println("failed to create initialize account for " + account.ID)
	return nil, errors.New("failed to initialize an account for " + account.ID + " => " + err.Error())

}

//GetAccount for user
/*

 */
func GetAccount(companyID string, stub shim.ChaincodeStubInterface) (Account, error) {
	var company Account
	companyBytes, err := stub.GetState(accountPrefix + companyID)
	if err != nil {
		fmt.Println("Account not found " + companyID)
		return company, errors.New("Account not found " + companyID)
	}

	err = json.Unmarshal(companyBytes, &company)
	if err != nil {
		fmt.Println("Error unmarshalling account " + companyID + "\n err:" + err.Error())
		return company, errors.New("Error unmarshalling account " + companyID)
	}

	return company, nil
}

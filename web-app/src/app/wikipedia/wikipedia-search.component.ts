import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { FormControl } from '@angular/forms';

import { WikipediaService } from './wikipedia.service';

@Component({
    selector: 'wikipedia-search',
    templateUrl: './wikipedia-search.component.html'
})

export class WikipediaSearchComponent implements OnInit {
    items: Observable<Array<string>>;
    term = new FormControl();

    constructor(private wikipediaService: WikipediaService) { }

    ngOnInit() { 
        this.items = this.term.valueChanges
                                .debounceTime(400)
                                .distinctUntilChanged()
                                .switchMap(term => this.wikipediaService.search(term));
    }

}
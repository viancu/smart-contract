import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Http, Headers } from '@angular/http';
import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Observable';


import { ChaincodeService } from '../chaincode/chaincode.service';
import { Account } from '../dashboard/account';

export class User {
    constructor(public username: string) { }
}



@Injectable()
export class AuthService {
    private loggedIn = false;
    authChange: Subject<boolean> = new Subject<boolean>();
    private hostnamme: string;
    constructor(private http: Http, private chaincodeService: ChaincodeService) {
        this.hostnamme = window.location.hostname;
        console.log(sessionStorage.getItem('isLoggedIn'));
        if (sessionStorage.getItem('isLoggedIn') == 'true') {
            this.loggedIn = true;
        }
    }

    getAccount(username: string): Observable<Account> {
        return this.chaincodeService.getAccount(username).map((message) => {
            return <Account>message;
        });
    }
    login(username: string): Observable<Account> {
        return this.chaincodeService.getAccount(username).map((message) => {
            sessionStorage.setItem('isLoggedIn', 'true')
            sessionStorage.setItem('username', username)
            this.loggedIn = true
            return <Account>message;
        });
    }

    logout() {
        sessionStorage.removeItem('isLoggedIn')
        sessionStorage.removeItem('username');
        this.loggedIn = false;
        
    }

    isLoggedIn() {
        console.log("is logged in " + this.loggedIn);
        this.authChange.next(this.loggedIn);
        return this.loggedIn;
    }
}
import { Component, } from '@angular/core';
import { Subject } from 'rxjs/Subject';
import { Router } from '@angular/router';

import { User } from './user';
import { AuthService } from './auth.service';
import { DashboardService } from '../dashboard/dashboard.service';
import { Account } from '../dashboard/account';

@Component({
    selector: 'login',
    templateUrl: 'login.component.html',
    styleUrls: ['./login.css'],

})
export class AuthComponent {
    submitted = false;
    
    username: string = "";
    password: string = "";
    constructor(private authService: AuthService, private dashboardService: DashboardService, private router: Router) { }

    login() {
        this.submitted = true;
        this.authService.login(this.username).subscribe((account) => {
            if (account) {
                
                //sessionStorage.setItem("account", result.message)
                this.dashboardService.account = account
                


                this.router.navigate(['dashboard']);
            }
        });

    }

}
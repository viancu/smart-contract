import { Component, AfterViewInit, OnInit } from '@angular/core';
import { AuthService } from './auth/auth.service';
import { Subscription }   from 'rxjs/Subscription';

@Component({
	selector: 'my-app',
	templateUrl: './app.component.html',
	styleUrls: ['./app.component.css']
})


export class AppComponent implements OnInit {
	title = 'Contracts';
	isLoggedIn: boolean;
	authSubscription: Subscription;

	constructor(private authService: AuthService) {
		this.authSubscription = authService.authChange.subscribe((value) => {
			this.isLoggedIn = value;
		});
	}
	ngOnInit() {
		this.isLoggedIn = this.authService.isLoggedIn();
	}

	ngOnDestroy() {
		this.authSubscription.unsubscribe();
	}

	logout() {
		this.authService.logout()
	}
}

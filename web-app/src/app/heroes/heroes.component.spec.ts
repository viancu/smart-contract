import { HeroesComponent } from './heroes.component';
import { HeroService } from './hero.service';

describe('Heroes Component Tests', () => {
    let heroesComponent: HeroesComponent;
    let heroService: HeroService = new HeroService();

    beforeEach(() => {
        heroesComponent = new HeroesComponent(heroService);
    });

	it('should have name property', () => {
        heroesComponent.ngOnInit();
		expect(heroesComponent.name).toBe('Johoon');
    });
});
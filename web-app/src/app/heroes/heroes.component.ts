import { Component, OnInit } from '@angular/core';


import { Hero } from './hero';
import { HeroService } from './hero.service';

@Component({
    selector: 'my-heroes',
    providers: [HeroService],
    templateUrl: './heroes.component.html',
    styleUrls: [
        './heroes.css'
    ]
})

export class HeroesComponent implements OnInit {
    heroes: Hero[];
    selectedHero: Hero;
    errorMessage: String;
    name: string = 'John';
    constructor(private heroService: HeroService) { }

    getHeroes() {
        this.heroService.getHeroes()
            .subscribe(
            heroes => this.heroes = heroes,
            error => this.errorMessage = <any>error);
    }

    ngOnInit(): void {
        this.getHeroes();
    }

    onSelect(hero: Hero): void {
        this.selectedHero = hero;
    }
}

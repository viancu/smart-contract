import { Account } from './account';

export class Contract {
    constructor(
        public cusip: string = "",
        public ticker: string = "",
        public par: number = 0,
        public qty: number = 0,
        public discount: number = 0,
        public maturity: number = 0,
        public owners: any[] = [],
        public issuer: string = "",
        public issueDate: string =  "",
        public validated: boolean = false
    ) { }
}
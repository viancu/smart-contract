export class Account {
    id: string = null
    prefix: string = null
    cashBalance: number = null
    assetsIds: number[] = null
}
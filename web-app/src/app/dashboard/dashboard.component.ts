import { Component, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { Router } from '@angular/router';
import { ModalComponent } from 'ng2-bs3-modal/ng2-bs3-modal';
import { Observable } from 'rxjs/Observable';


import { DashboardService } from '../dashboard/dashboard.service';
import { Account } from '../dashboard/account';
import { ChaincodeService } from '../chaincode/chaincode.service';
import { Contract } from './contract'

@Component({
    selector: 'contract-form',
    templateUrl: './contract-form.component.html',
    styles: [
        `.ng-valid[required] {
            border-left: 5px solid #5cb85c; /* green */
        }`,
        `.ng-invalid:not(.ng-untouched):not(form) {
            border-left: 5px solid #d9534f; /* red */
        }`,
        `.red-text {
            color: #d9534f !important; /* red */
        }`
    ],
    encapsulation: ViewEncapsulation.None
})

export class ContractFormComponent {

    @ViewChild('modal')
    modal: ModalComponent
    contractModel: Contract = new Contract()
    tickers: any[] = [
        { key: "OJ", value: "Orange Juice" },
        { key: "CO", value: "Cocoa" },
        { key: "CT", value: "Cotton" }
    ]
    discounts: any[] = [
        { key: 2.5, value: "2.5%" },
        { key: 5, value: "5%" },
        { key: 7.5, value: "7.5%" },
    ]
    maturities: any[] = [
        { key: 7, value: "7 days" },
        { key: 15, value: "15 days" },
        { key: 30, value: "30 days" }
    ]

    constructor(private chaincodeService: ChaincodeService) { }

    closed() {

        this.contractModel.issuer = sessionStorage.getItem("username")
        let date = new Date()
        this.contractModel.discount = parseFloat(this.contractModel.discount + "")
        this.contractModel.par = parseFloat(this.contractModel.par + "")
        this.contractModel.qty = parseFloat(this.contractModel.qty + "")
        this.contractModel.maturity = parseFloat(this.contractModel.maturity + "")
        this.contractModel.issueDate = (new Date).getTime() + ""
        this.chaincodeService.createContract(this.contractModel).subscribe((result) => {
            this.contractModel = new Contract()
        })

    }

    dismissed() {
        this.contractModel = new Contract()
    }
}

@Component({
    selector: 'my-dashboard',
    templateUrl: './dashboard.component.html',
    styleUrls: ['./dashboard.css'],
})
export class DashboardComponent implements OnInit {
    @ViewChild(ContractFormComponent)
    contractForm: ContractFormComponent
    @ViewChild('modal')
    modal: ModalComponent
    account: Account
    contracts: Contract[]
    test: string
    transfer = {
        "cusip": "",
        "fromCompany": "",
        "toCompany": "",
        "quantity": 0
    }
    constructor(private dashboardService: DashboardService, private chaincodeService: ChaincodeService) {
        this.account = dashboardService.account
    }

    ngOnInit() {
        var username = sessionStorage.getItem('username');
        if (username) {
            this.initializeAccountPolling(username).subscribe(
                (account) => {
                    this.account = account
                    this.chaincodeService.getContracts().subscribe(
                        (contracts) => {
                            this.contracts = contracts
                        }
                    )
                }
            )

        }
    }

    initializeContractPolling() {
        return Observable
            .interval(2000)
            .flatMap(() => {
                return this.chaincodeService.getContracts()
            });
    }

    initializeAccountPolling(username:string) {
        return Observable
            .interval(2000)
            .flatMap(() => {
                return this.chaincodeService.getAccount(username)
            });
    }

    closed() {
        this.transfer.quantity = parseInt(this.transfer.quantity + "")
        this.chaincodeService.transferContract(this.transfer).subscribe(
            () => {
                this.transfer = {
                    "cusip": "",
                    "fromCompany": "",
                    "toCompany": "",
                    "quantity": 0
                }
            }
        )
    }

    validate(cusip: string) {
        this.chaincodeService.validateContract(cusip).subscribe()
    }

    dismissed() {
        this.transfer = {
            "cusip": "",
            "fromCompany": "",
            "toCompany": "",
            "quantity": 0
        }
    }

    open(cusip: string, issuer: string) {
        this.transfer.cusip = cusip
        this.transfer.fromCompany = issuer
        this.transfer.toCompany = this.account.id
        this.modal.open()
    }

}

@Component({
    selector: 'dashboard-login',
    templateUrl: './dashboard-login.component.html'
})
export class DashboardLogin {
    public response: String

    constructor(private http: Http) { }

    getJson() {
        this.http.get('https://60eb57ac.ngrok.io/chain')
            .map(res => res.text())
            .subscribe(
            data => this.response = data,
            err => this.logError(err),
            () => console.log('Random Quote Complete')
            );
    }

    logError(error: any) {
        console.error('There was an error: ' + error);
    }
}



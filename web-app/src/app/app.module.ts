import { NgModule , Provider} from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { HttpModule, JsonpModule } from '@angular/http';
import { routing } from './app.routing';
import { ReactiveFormsModule } from '@angular/forms';
import { Ng2Bs3ModalModule } from 'ng2-bs3-modal/ng2-bs3-modal';

import { AppComponent } from './app.component';
import { HeroDetailComponent } from './heroes/hero-detail.component';
import { HeroesComponent } from './heroes/heroes.component';
import { HeroService } from './heroes/hero.service';
import { DashboardComponent, DashboardLogin, ContractFormComponent } from './dashboard/dashboard.component';
import { DashboardService } from './dashboard/dashboard.service';

import { AuthComponent } from './auth/auth.component';
import { AuthService } from './auth/auth.service';
import { ChaincodeService } from './chaincode/chaincode.service';

import { CanActivateViaAuthGuard } from './auth/guard';

@NgModule({
	imports: [
		BrowserModule,
		FormsModule,
		HttpModule,
		JsonpModule,
		ReactiveFormsModule,
		Ng2Bs3ModalModule,
		routing
	],
	declarations: [
		AppComponent,
		HeroDetailComponent,
		HeroesComponent,
		DashboardComponent,
		DashboardLogin,
		AuthComponent,
		ContractFormComponent
	],
	providers: [
		HeroService,
		AuthService,
		ChaincodeService,
		DashboardService,
		CanActivateViaAuthGuard
	],
	bootstrap: [AppComponent]
})
export class AppModule { }

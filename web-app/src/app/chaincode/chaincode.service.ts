import { Injectable } from '@angular/core';
import { URLSearchParams, Jsonp } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { Http, Response } from '@angular/http';
import { ChaincodeResponse } from './chaincode-response'
import { Account } from '../dashboard/account';
import { Contract } from '../dashboard/contract';

import { Request } from './request';

@Injectable()
export class ChaincodeService {
    private hostname: string;

    constructor(private http: Http) {
        this.hostname = 'http://' + window.location.hostname + ':7050/';
    }

    public getAccount(username: string): Observable<Account> {
        var body = JSON.stringify(new Request("query", ["getAccount", username]));
        return this.http.post(this.hostname + 'chaincode', body)
            .map((res) => {
                let response = res.json()
                if(response.result.message) {
                    return JSON.parse(response.result.message)
                }
            })
            .catch((error: any) => Observable.throw(error.error || 'Server error'));
    }
    public getContracts(): Observable<Contract[]> {
        var body = JSON.stringify(new Request("query", ["getAllContracts"]));
        return this.http.post(this.hostname + 'chaincode', body)
            .map((res) => {
                let message = JSON.parse(res.json().result.message)
                if(message != null) {
                    return message.reverse()
                } else {
                    return []
                }
            })
    }

    public createContract(contract : Contract ): Observable<string> {
        var body = JSON.stringify(new Request("invoke", ["createContract", JSON.stringify(contract)]))
         
        return this.http.post(this.hostname + 'chaincode', body)
            .map((res) => {
                let response = res.json()
                if(response.result.message) {
                    return response.result.message
                }
            })
            .catch((error: any) => Observable.throw(error.json() || 'Server error'));
    }

    public transferContract(transfer : Object ): Observable<string> {
        var body = JSON.stringify(new Request("invoke", ["transferContract", JSON.stringify(transfer)]))
         
        return this.http.post(this.hostname + 'chaincode', body)
            .map((res) => {
                let response = res.json()
                if(response.result.message) {
                    return response.result.message
                }
            })
            .catch((error: any) => Observable.throw(error.json() || 'Server error'));
    }

    public validateContract(cusip : string ): Observable<string> {
        var body = JSON.stringify(new Request("invoke", ["validateContract", cusip]))
         
        return this.http.post(this.hostname + 'chaincode', body)
            .map((res) => {
                return ""
            })
            .catch((error: any) => Observable.throw(error.json() || 'Server error'));
    }
}
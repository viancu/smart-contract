export class ChaincodeResponse {

    constructor(
        jsonrpc: string,
        result: {
            status: "",
            message: ""
        },
        id: number
    ) {
        if (result.message) {
            result.message = JSON.parse(result.message)
        }
    }
}
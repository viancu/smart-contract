export class Request {
    jsonrpc = "2.0"
    method: string
    params = {
        "type": 1,
        "chaincodeID": {
            "name": "SmartContractChaincode"
        },
        "ctorMsg": {
            "args": [""]
        }
    }
    id : number = 100

    constructor(method = "query", args = ["generic"]) {
        this.method = method
        this.params.ctorMsg.args = args
    }
}